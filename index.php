<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC A02</title>
</head>
<body>
	<h1>Divisibles of Five</h1>
	<?php forLoop(); ?>

	<h1>Array Manipulation</h1>
	<?php array_push($students, 'John Smith'); ?>
	<?php var_dump($students); ?>
	<p><?= count($students); ?></p>

	<?php array_push($students, 'Jane Smith'); ?>
	<?php var_dump($students); ?>
	<p><?= count($students); ?></p>

	<?php array_shift($students); ?>
	<?php var_dump($students); ?>
	<p><?= count($students); ?></p>

</body>
</html>